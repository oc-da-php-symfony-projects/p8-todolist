# ToDoList White Book
This document will guide you through the contribution process.
## Versioning: Git
The project is hosted [there](https://gitlab.com/oc-da-php-symfony-projects/p8-todolist).

Installation guide [here](../master/README.md)
### Issue/Branch/MR
>__Before any change or contribution in the code, please create an issue prefixed following the Git Flow standards.__

- « Feature » for new feature development.
- « Release » to push a new version.
- « Hotfix » to fix a production bug.

Fill the description field with some information about the changes, why you made them, and how it improves the application.

Create a Merge Request prefixed with WIP (Work In Process) status.

Then checkout the newly created branch on your local machine and start working on it.

> Don't forget to often commit and push your changes, but before you push,
> to ensure that your code is up with the quality standards and bug free,
> run the [Linters](#Linters) and [Tests](#Tests) .
>
> CI takes some time to process, and you won't be happy after waiting for the pipeline to run, to push again for a single forgotten space before a comma. 
> 
> And you certainly will ;-)
### CI/CD
Now, each time you push a commit, the CI/CD is triggered.

This is done by adding a .gitlab-ci file at the root of the project and pushing it to gitlab.com repository.

In this file, 'jobs' are settled and run by gitlab-runner using Gitlab's docker shared runner on remote repository.

It is possible to install and configure a private runner on your machine for local testing. 

To do so see [docs.gitlab.com/runner](https://docs.gitlab.com/runner/)

When you're ready, and pipeline pass, ask for merging or merge it yourself.

## Project Description
### Files
- Configuration files are located in `/config` directory.

  `/config/services.yaml` is where you can mainly find configuration spots.
- CSS, JS, Images, and fonts are located in `/public` directory.

  Anything that should be accessed from the 'outside' should be placed here.
- Application files are located in `/src` directory.

    - `/src/Entity` Directory contains entities object for doctrine mapping.

      Mapping configuration can be changed in `/config/package.doctrine.yaml`.

      If you do that, be aware to add the corresponding tag to PSR autoload in composer.json.
    - `/src/Controller` Directory contains Controllers that manage requests and responses for the app.
    - `/src/Form` Directory contains FormTypes files for the applications symfony form.
    - `/src/Repository` Directory contains repositories bound with entities in doctrine for easiest queries.
    - `/src/Security` Directory contains The Security Authenticator, and `/src/Security/Voters` directory in which voters determining the access strategy are located.
### Linters

Linters are fine helpers to keep the code clean.

You can run it either separately or together.

To run it individually, do
- Php Code Sniffer  : `composer run phpcs`
- Php Mess Detector : `composer run phpmd`
- Php Stan : `composer run phpstan`

Together: `composer run linter`

### Tests
There is a set of tests with full coverage for unit and functional tests to help you maintain the code bug free.

PhpUnit Unit Testing : `composer run utst`
PhpUnit Functional Testing : `composer run ftst`

Together : `composer run tests`

When adding new feature, don't forget to add corresponding tests.
The best recommended practice is to write test before coding.
There is a behat skeleton created that needs improvement but can be a good star for TDD training 

To run the  whole ci : `composer run ci`


