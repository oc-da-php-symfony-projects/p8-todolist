<?php

require __DIR__.'/vendor/autoload.php';
require __DIR__.'/vendor/friends-of-behat/mink-browserkit-driver/tests/Custom/ErrorHandlingTest.php';

use Behat\Mink\Driver\BrowserKitDriver;
use Behat\Mink\Session;



$client = new Behat\Mink\Tests\Driver\Custom\TestClient();
$driver = new BrowserKitDriver($client);
$session = new Session($driver);

$session->visit('https://127.0.0.1:8000');
echo "Status code: ". $session->getStatusCode() . "\n";
echo "Current URL: ". $session->getCurrentUrl() . "\n";
