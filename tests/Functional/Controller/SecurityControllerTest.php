<?php

namespace App\Tests\Functional\Controller;

use App\DataFixtures\AppFixtures;
use App\Repository\UserRepository;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken;

class SecurityControllerTest extends WebTestCase
{
    use FixturesTrait;

    private KernelBrowser $client;

    protected function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();
        $this->loadFixtures([
            AppFixtures::class,
        ]);
    }

    public function testLoginWithValidCredentials(): void
    {
        $crawler = $this->client->request('GET', '/login');
        $this->assertResponseIsSuccessful();
        $userRepository = static::$container->get(UserRepository::class);
        $user = $userRepository->findOneBy(['username' => 'TdlAdmin']);
        $session = self::$container->get('session');

        $token = new PostAuthenticationGuardToken($user, 'app_user_provider', $user->getRoles());
        $firewallContext = 'secured_area';

        $form = $crawler->selectButton('Se connecter')->form([
            'username' => 'TdlAdmin',
            'password' => 'TdlAdmin',
        ], 'POST');
        $this->client->submit($form);

        $session->set('_security_' . $firewallContext, serialize($token));
        $session->save();
        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
        $this->assertResponseRedirects('/', 302);
    }

    public function testLoginWithInvalidCredentials(): void
    {
        $this->client->followRedirects(true);
        $crawler = $this->client->request('GET', '/login');
        $form = $crawler->selectButton('Se connecter')->form([
            'username' => 'TdlAdmin',
            'password' => 'potatoes',
        ], 'POST');
        $this->client->submit($form);
        $this->assertRouteSame('app_login');
        $this->assertSelectorExists('div:contains("Invalid credentials.")');
    }

    public function testLoginWithUserNotFound(): void
    {
        $this->client->followRedirects(true);
        $crawler = $this->client->request('GET', '/login');
        $this->assertResponseIsSuccessful();
        $form = $crawler->selectButton('Se connecter')->form([
            'username' => 'toto',
            'password' => 'banana',
        ], 'POST');
        $this->client->submit($form);
        $this->assertSelectorExists('div:contains("Account could not be found.")');
    }

    /**
     * @covers \App\Controller\SecurityController::logout
     */
    public function testLogout(): void
    {
        $userRepository = static::$container->get(UserRepository::class);
        $user = $userRepository->findOneBy(['username' => 'TdlAdmin']);
        $this->client->loginUser($user);
        $crawler = $this->client->request('GET', '/');
        $link = $crawler->selectLink('Se déconnecter')->link();
        $this->assertTrue('/logout' == $link->getNode()->getAttribute('href'));
        $this->assertTrue('GET' == $link->getMethod());
        $this->client->click($link);
        $this->assertRouteSame('app_logout');
        $this->assertResponseRedirects('http://localhost/', 302);
    }
}
