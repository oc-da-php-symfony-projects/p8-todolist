<?php

namespace App\Tests\Functional\Controller;

use App\DataFixtures\AppFixtures;
use App\Entity\Task;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TaskControllerTest extends WebTestCase
{
    use FixturesTrait;

    private KernelBrowser $client;

    protected function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();
        $this->loadFixtures([
            AppFixtures::class,
        ]);
    }

    public function testListActionWhileNotLoggedIn(): void
    {
        $this->client->request('GET', '/tasks');
        $this->assertResponseRedirects('/login', 302);
    }

    public function testListActionWhileLoggedIn(): void
    {
        $userRepository = static::$container->get(UserRepository::class);
        $testUser = $userRepository->findOneBy(['username' => 'laura']);
        $this->client->loginUser($testUser);
        $this->client->request('GET', '/tasks');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Liste des Tâches');
    }

    public function testCreateActionWhileNotLoggedIn(): void
    {
        $this->client->request('GET', '/tasks/create');
        $this->assertResponseRedirects('/login', 302);
    }

    public function testCreateActionWhileLoggedIn(): void
    {
        $userRepository = static::$container->get(UserRepository::class);
        $testUser = $userRepository->findOneBy(['username' => 'laura']);
        $this->client->loginUser($testUser);
        $this->client->request('GET', '/tasks/create');
        $this->client->submitForm('Ajouter', [
            'task[title]' => 'My New Task',
            'task[content]' => 'My new task content'
        ], 'POST');
        $this->assertResponseRedirects('/tasks', 302);
    }

    public function testEditActionWhileNotLoggedIn(): void
    {
        $this->client->request('GET', '/tasks/3/edit');
        $this->assertResponseRedirects('/login', 302);
    }

    public function testEditAnonymousTaskWhileNotGrantedTaskEdit(): void
    {
        $userRepository = static::$container->get(UserRepository::class);
        $testUser = $userRepository->findOneBy(['username' => 'laura']);
        $this->client->loginUser($testUser);
        $this->client->request('GET', '/tasks/18/edit');

        $this->assertResponseStatusCodeSame(403, 'User tried to edit task without proper authorization');
    }

    public function testEditAnonymousTaskWhileGrantedTaskEdit(): void
    {
        $userRepository = static::$container->get(UserRepository::class);
        $testUser = $userRepository->findOneBy(['username' => 'TdlAdmin']);
        $this->client->loginUser($testUser);
        $task = self::getContainer()->get('doctrine.orm.entity_manager')->getRepository(Task::class)->find(18);
        $crawler =  $this->client->request('GET', '/tasks/18/edit');
        $form = $crawler->selectButton('Modifier')->form();
        $this->assertTrue($form['task[title]']->getValue() == $task->getTitle());
        $this->assertTrue($form['task[content]']->getValue() == $task->getContent());
        $this->client->submit($form, [
            'task[title]' => 'My new Title',
            'task[content]' => $task->getContent()
        ]);
        $this->assertResponseRedirects('/tasks', 302);
    }

    public function testEditTaskWhileNotGrantedTaskEdit(): void
    {
        $userRepository = static::$container->get(UserRepository::class);
        $testUser = $userRepository->findOneBy(['username' => 'laura']);
        $this->client->loginUser($testUser);
        $this->client->request('GET', '/tasks/3/edit');
        $this->assertResponseStatusCodeSame(403, 'User tried to edit task without proper authorization');
    }

    public function testEditTaskWhileGrantedTaskEdit(): void
    {
        $userRepository = static::$container->get(UserRepository::class);
        $testUser = $userRepository->findOneBy(['username' => 'laura']);
        $this->client->loginUser($testUser);
        $task = self::getContainer()->get('doctrine.orm.entity_manager')->getRepository(Task::class)->find(1);
        $crawler =  $this->client->request('GET', '/tasks/1/edit');
        $form = $crawler->selectButton('Modifier')->form();
        $this->assertTrue($form['task[title]']->getValue() == $task->getTitle());
        $this->assertTrue($form['task[content]']->getValue() == $task->getContent());
        $this->client->submit($form, [
            'task[title]' => 'My New Test Title',
            'task[content]' => $task->getContent()
        ]);
        $this->assertResponseRedirects('/tasks', 302);
    }

    public function testToggleTaskWhileNotGrantedTaskEdit(): void
    {
        $userRepository = static::$container->get(UserRepository::class);
        $testUser = $userRepository->findOneBy(['username' => 'laura']);
        $this->client->loginUser($testUser);
        $this->client->request('GET', '/tasks/3/toggle');
        $this->assertResponseStatusCodeSame(403, 'User tried to edit task without proper authorization');
    }
//
    public function testToggleTaskWhileGrantedTaskEdit(): void
    {
        $userRepository = static::$container->get(UserRepository::class);
        $testUser = $userRepository->findOneBy(['username' => 'laura']);
        $this->client->loginUser($testUser);
        $crawler =  $this->client->request('GET', '/tasks/1/toggle');
        $this->assertResponseRedirects('/tasks', 302);
    }

    public function testDeleteActionWhileNotLoggedIn(): void
    {
        $this->client->request('GET', '/tasks/3/delete');
        $this->assertResponseRedirects('/login', 302);
    }

    public function testDeleteActionWhileNotGrantedTaskEdit(): void
    {
        $userRepository = static::$container->get(UserRepository::class);
        $testUser = $userRepository->findOneBy(['username' => 'laura']);
        $this->client->loginUser($testUser);
        $this->client->request('GET', '/tasks/3/delete');
        $this->assertResponseStatusCodeSame(403, 'User tried to delete task without proper authorization');
    }

    public function testDeleteActionWhileGrantedTaskEdit(): void
    {
        $userRepository = static::$container->get(UserRepository::class);
        $testUser = $userRepository->findOneBy(['username' => 'TdlAdmin']);
        $this->client->loginUser($testUser);
        $this->client->request('GET', '/tasks/3/delete');
        $this->assertResponseStatusCodeSame(302);
    }
}
