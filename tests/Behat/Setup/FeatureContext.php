<?php

namespace App\Tests\Behat\Setup;

use App\Entity\Task;
use App\Entity\User;
use App\Tests\Behat\Page\Task\IndexPage;
use Behat\Behat\Context\Context;

use Behat\Gherkin\Node\TableNode;
use Doctrine\Persistence\ObjectManager;
use FriendsOfBehat\PageObjectExtension\Page\UnexpectedPageException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

class FeatureContext implements Context
{
    private ObjectManager $objectManager;

    private UserPasswordEncoder $encoder;

    private IndexPage $indexPage;

    public function __construct(ObjectManager $objectManager, UserPasswordEncoder $encoder, IndexPage $indexPage)
    {
        $this->objectManager = $objectManager;
        $this->encoder = $encoder;
        $this->indexPage = $indexPage;
    }

	/**
	 * @Given /^user is on IndexPage$/
	 * @throws UnexpectedPageException
	 */
    public function userIsOnIndexPage()
    {
        $this->indexPage->open();
    }

    /**
     * @Given /^user "([^"]+)" is registered with email "([^"]+)" and password "([^"]+)"$/
     */
    public function registerUser(string $username, string $email, string $password)
    {
        $user = new User();
        $user->setEmail($email);
        $user->setUsername($username);
        $user->setPassword($this->encoder->encodePassword($user, $password));
        $this->objectManager->persist($user);
        $this->objectManager->flush();
    }

    /**
     * @Given /^the following people exist:$/
     */
    public function theFollowingPeopleExist(TableNode $table)
    {
        foreach ($table as $row) {
            $this->registerUser($row['username'], $row['email'], $row['password']);
        }
//        throw new PendingException();
    }

    /**
     * @When /^user create Task with "([^"]*)" and "([^"]*)"$/
     */
    public function userCreateTaskWithAnd(string $title, string $content): Task
    {
        $task = new Task();
        $task->setTitle($title);
        $task->setContent($content);
        $this->objectManager->persist($task);
        $this->objectManager->flush();

        return $task;
    }

    /**
     * @Given /^the following tasks exist:$/
     */
    public function theFollowingTasksExist(TableNode $table): void
    {
        foreach ($table as $row) {
            $task = $this->userCreateTaskWithAnd($row['title'], $row['content']);
            if (!empty($row['user'])) {
                $task->setUser(
                    $this->objectManager
                        ->getRepository('App:User')
                        ->findOneBy(['username' => $row['user']])
                );
            }
            $this->objectManager->persist($task);
        }
        $this->objectManager->flush();
    }
}
