<?php

namespace App\Tests\Behat\Context\Task;

use App\Entity\Task;
use App\Tests\Behat\Page\Task\CreateTaskPage;
use App\Tests\Behat\Page\Task\TaskEditPage;
use App\Tests\Behat\Page\Task\TaskPage;
use Behat\Behat\Context\Context;
use FriendsOfBehat\PageObjectExtension\Page\UnexpectedPageException;

class TaskContext implements Context
{
    private TaskPage $taskPage;
    private CreateTaskPage $createTaskPage;
    private TaskEditPage $taskEditPage;

    public function construct(TaskPage $taskPage, CreateTaskPage $createTaskPage, TaskEditPage $taskEditPage)
    {
        $this->createTaskPage = $createTaskPage;
        $this->taskPage = $taskPage;
        $this->taskEditPage = $taskEditPage;
    }

    /**
     * @Then /^user should be on CreateTaskPage$/
     *
     * @throws UnexpectedPageException
     */
    public function userShouldBeOnCreateTaskPage()
    {
        $this->createTaskPage->verify();
    }

    /**
     * @Then /^user should be on Task :task Page$/
     *
     * @throws UnexpectedPageException
     */
    public function userShouldBeOnTaskPage(Task $task)
    {
        $this->taskPage->verify(['id' => $task->getId()]);
    }

    /**
     * @Then /^user should be on Task :task EditPage$/
     *
     * @throws UnexpectedPageException
     */
    public function userShouldBeOnTaskEditPage(Task $task)
    {
        $this->taskEditPage->verify(['id' => $task->getId()]);
    }

//    /**
//     * @Then /^user should see creation form$/
//     */
//    public function userShouldSeeCreationForm()
//    {
////        throw new PendingException();
//        return true;
//    }
//
//    /**
//     * @Then /^user should see "([^"]*)"$/
//     *
//     * @param mixed $arg1
//     */
//    public function userShouldSee($arg1)
//    {
//        return true;
//        //		throw new PendingException();
//    }
}
