<?php

namespace App\Tests\Behat\Context\Task;

use App\Entity\Task;
use App\Tests\Behat\Page\Task\CreateTaskPage;
use App\Tests\Behat\Page\Task\TaskEditPage;
use App\Tests\Behat\Page\Task\TaskListPage;
use App\Tests\Behat\Page\Task\TaskPage;
use Behat\Behat\Context\Context;
use FriendsOfBehat\PageObjectExtension\Page\UnexpectedPageException;

class TaskListContext implements Context
{
    private TaskListPage $taskListPage;
    private TaskPage $taskPage;
    private TaskEditPage $taskEditPage;
    private CreateTaskPage $createTaskPage;

    public function __construct(
        TaskListPage $taskListPage,
        TaskPage $taskPage,
        TaskEditPage $taskEditPage,
        CreateTaskPage $createTaskPage
    ) {
        $this->taskListPage = $taskListPage;
        $this->taskPage = $taskPage;
        $this->taskEditPage = $taskEditPage;
        $this->createTaskPage = $createTaskPage;
    }

    /**
     * @Given /^user is on TaskListPage$/
     *
     * @throws UnexpectedPageException
     */
    public function userIsOnTaskListPage()
    {
        $this->taskListPage->open();
    }

    /**
     * @Then user should be on TaskListPage
     *
     * @throws UnexpectedPageException
     */
    public function userShouldBeOnTaskListPage()
    {
        $this->taskListPage->verify();
    }

    /**
     * @When /^user click add task link$/
     *
     * @throws UnexpectedPageException
     */
    public function userClickAddTaskLink()
    {
        $this->createTaskPage->open();
    }

    /**
     * @When /^user click edit :task link$/
     *
     * @throws UnexpectedPageException
     */
    public function userClickEditLink(Task $task)
    {
        $this->taskEditPage->open(['id' => $task->getId()]);
    }
}
