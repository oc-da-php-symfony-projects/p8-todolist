<?php

namespace App\Tests\Behat\Context;

use App\Tests\Behat\Page\LoginPage;
use Behat\Behat\Context\Context;
use FriendsOfBehat\PageObjectExtension\Page\UnexpectedPageException;

class LoginContext implements Context
{
    private LoginPage $loginPage;

    public function __construct(LoginPage $loginPage)
    {
        $this->loginPage = $loginPage;
    }

    /**
     * @Given /^user is on Login page$/
     *
     * @throws UnexpectedPageException
     */
    public function userIsOnLoginPage()
    {
        $this->loginPage->open();
    }

    /**
     * @When user log in as :username with :password
     */
    public function login(string $username, string $password)
    {
        $this->loginPage->login($username, $password);
    }
}
