<?php

namespace App\Tests\Behat\Context\User;

use Behat\Behat\Context\Context;
use Behat\Behat\Tester\Exception\PendingException;

class UserContext implements Context
{
	/**
	 * @Given /^user is logged in as $/
	 */
	public function userIsLoggedIn()
	{
		throw new PendingException();
	}
}
