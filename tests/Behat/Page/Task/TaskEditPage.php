<?php

namespace App\Tests\Behat\Page\Task;

use FriendsOfBehat\PageObjectExtension\Page\SymfonyPage;

class TaskEditPage extends SymfonyPage
{
    public function getRouteName(): string
    {
        return 'task_edit';
    }
}
