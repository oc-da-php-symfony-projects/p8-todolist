<?php

namespace App\Tests\Behat\Page\Task;

use FriendsOfBehat\PageObjectExtension\Page\SymfonyPage;

class IndexPage extends SymfonyPage
{
    public function getRouteName(): string
    {
        return 'homepage';
    }
}
