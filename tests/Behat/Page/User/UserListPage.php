<?php

namespace App\Tests\Behat\Page\User;

use FriendsOfBehat\PageObjectExtension\Page\SymfonyPage;

class UserListPage extends SymfonyPage
{
    public function getRouteName(): string
    {
        return 'user_list';
    }
}
