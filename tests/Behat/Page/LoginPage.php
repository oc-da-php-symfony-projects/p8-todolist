<?php

namespace App\Tests\Behat\Page;

use FriendsOfBehat\PageObjectExtension\Page\SymfonyPage;

class LoginPage extends SymfonyPage
{
    public function getRouteName(): string
    {
        return 'app_login';
    }

    public function login($user, $password)
    {
        $this->open();
        $this->getDocument()->fillField('username', $user);
        $this->getDocument()->fillField('password', $password);
        $this->getDocument()->pressButton('Se connecter');
    }
}
