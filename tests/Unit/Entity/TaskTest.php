<?php

namespace App\Tests\Unit\Entity;

use App\Entity\Task;
use App\Entity\User;
use DateTime;
use Faker\Factory;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validation;

class TaskTest extends TestCase
{
//    protected array $rolesUser;
//    protected array $rolesAdmin;
//    protected ?array $roles;
//    private User $user;
//
//    protected function setUp(): void
//    {
//        $this->user = new User();
//    }

    public function getEntity(): Task
    {
        return new Task();
    }

    public function testConstruct(): void
    {
        static::assertObjectHasAttribute('createdAt', $this->getEntity(), 'missing attribute "createdAt"');
        static::assertObjectHasAttribute('isDone', $this->getEntity(), 'missing attribute "isDone"');
    }

    public function testEntityIsValid(): void
    {
        $faker = Factory::create();
        $task = $this->getEntity();
        static::assertTrue(null == $task->getId());
        static::assertInstanceOf(DateTime::class, $task->getCreatedAt());
        $task->setContent($faker->text());
        $task->setTitle($faker->text(50));
        $this->assertHasErrors($task, 0);
    }

    public function testInvalidEntity()
    {
        $task = $this->getEntity();
        $this->assertHasErrors($task, 2);
    }

    public function assertHasErrors(Task $task, int $expectedErrors)
    {
        $validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping(true)
            ->addDefaultDoctrineAnnotationReader()
            ->getValidator()
        ;
        /**
         * @var ConstraintViolationList $errors
         */
        $errors = $validator->validate($task);

        $messages = [];
        /** @var ConstraintViolation $error */
        foreach ($errors as $error) {
            $messages[] = $error->getPropertyPath() . ' => ' . $error->getMessage();
        }
        static::assertCount($expectedErrors, $errors, implode(', ', $messages));
    }

    public function testIsDone()
    {
        $task = $this->getEntity();
        static::assertIsBool($task->isDone());
    }

    /**
     * @dataProvider flag
     *
     * @param mixed $flag
     */
    public function testToggle($flag)
    {
        $task = $this->getEntity();
        $task->toggle($flag);
        static::assertSame($flag, $task->isDone());
    }

    public function flag()
    {
        return [
            [true],
        ];
    }
}
