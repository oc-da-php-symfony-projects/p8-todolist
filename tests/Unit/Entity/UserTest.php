<?php

namespace App\Tests\Unit\Entity;

use App\Entity\Task;
use App\Entity\User;
use JetBrains\PhpStorm\Pure;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validation;

class UserTest extends KernelTestCase
{
    private const VALID_EMAIL_VALUE = 'rococo@geail.com';
    private const VALID_PASSWORD_VALUE = 'rococorococo';
    private const VALID_USERNAME_VALUE = 'rococo';
    protected array $rolesUser;
    protected array $rolesAdmin;
    protected ?array $roles;
    protected PasswordEncoderInterface | MockObject $encoder;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
        $this->encoder = $this->createMock(UserPasswordEncoderInterface::class);
        $this->rolesAdmin = ['ROLE_USER', 'ROLE_ADMIN'];
    }

    #[Pure] public function getEntity(): User
    {
        return new User();
    }

    public function testConstruct(): void
    {
        static::assertObjectHasAttribute('tasks', $this->getEntity(), 'missing attribute "tasks"');
    }

    public function testEntityIsValid(): void
    {
        $user = $this->getEntity();
        $user->setEmail(self::VALID_EMAIL_VALUE);
        $user->setUsername(self::VALID_USERNAME_VALUE);
        $user->setPassword(self::VALID_PASSWORD_VALUE);
        static::assertSame($user->getEmail(), self::VALID_EMAIL_VALUE);
        static::assertHasErrors($user, 0);
        static::assertTrue(null == $user->getId());
        static::assertTrue(null == $user->getSalt());
        static::assertTrue(null == $user->eraseCredentials());
    }

    public function testInvalidEntity()
    {
        $user = $this->getEntity();
        $this->assertHasErrors($user, 3);
    }

    public function assertHasErrors(User $user, int $expectedErrors)
    {
        $validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping(true)
            ->addDefaultDoctrineAnnotationReader()
            ->getValidator()
        ;
        /**
         * @var ConstraintViolationList $errors
         */
        $errors = $validator->validate($user, null, ['registration']);

        $messages = [];
        /** @var ConstraintViolation $error */
        foreach ($errors as $error) {
            $messages[] = $error->getPropertyPath() . ' => ' . $error->getMessage();
        }
        static::assertCount($expectedErrors, $errors, implode(', ', $messages));
    }

    public function getId(): void
    {
        $user = $this->getEntity();
        static::assertNull($user->getId());
    }

    public function testGetUsername(): void
    {
        $user = $this->getEntity()->setUsername(self::VALID_USERNAME_VALUE);
        static::assertSame($user->getUsername(), self::VALID_USERNAME_VALUE);
    }

    /**
     * @dataProvider task
     */
    public function testTask($task): void
    {
        $user = $this->getEntity();
        static::assertSame('Task3', $task->getTitle());
        static::assertSame('Task3ContentTask3Content', $task->getContent());
        static::assertInstanceOf(Task::class, $task);
        if (!$user->getTasks()->contains($task)) {
            $user->addTask($task);
        }
        static::assertTrue($user->getTasks()->contains($task));
        static::assertSame($task->getUser(), $user);
        $user->removeTask($task);
        static::assertTrue(!$user->getTasks()->contains($task));
    }


    public function getSalt(): null | string
    {
        return null;
    }

    public function testGetPassword(): void
    {
        $user = $this->getEntity();
        $user->setPassword(self::VALID_PASSWORD_VALUE);
        static::assertSame($user->getPassword(), self::VALID_PASSWORD_VALUE);
    }

    public function eraseCredentials()
    {
    }

    /**
     * @dataProvider rolesForAdmin
     */
    public function testSetRoles(?array $roles): void
    {
        $user = $this->getEntity();
        $user->setRoles($roles);
        foreach ($roles as $role) {
            static::assertContains($role, $user->getRoles());
        }
    }

    /**
     * @dataProvider admin
     */
    public function testRemoveRole(mixed $role): void
    {
        $user = $this->getEntity();
        $user->setRoles($this->rolesAdmin);
        static::assertContains($role, $user->getRoles());
        $user->removeRole($role);
        static::assertNotContains($role, $user->getRoles());
    }

    public function rolesForAdmin(): array
    {
        return [
            [['ROLE_USER', 'ROLE_ADMIN']],
        ];
    }

    public function admin(): array
    {
        return [
            ['ROLE_ADMIN'],
        ];
    }

    public function task(): array
    {
        return [
            [(new Task())->setTitle('Task3')->setContent('Task3ContentTask3Content')],
        ];
    }
}
