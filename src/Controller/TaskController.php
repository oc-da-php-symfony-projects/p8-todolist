<?php

namespace App\Controller;

use App\Entity\Task;
use App\Entity\User;
use App\Form\TaskType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TaskController
 * @package App\Controller
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class TaskController extends AbstractController
{
    /**
     * @Route("/tasks", name="task_list", methods={"GET"})
     */
    public function listAction(): Response
    {
        $this->denyAccessUnlessGranted('TASK', null, 'User tried to access tasks list without being authenticated');
        return $this->render('task/list.html.twig', [
                'tasks' => $this->getDoctrine()->getRepository(Task::class)->findAll(),
            ]);
    }

    /**
     * @Route("/tasks/create", name="task_create")
     */
    public function createAction(Request $request): RedirectResponse | Response
    {
        $this->denyAccessUnlessGranted('TASK', null, 'User tried to create task without being authenticated');
        $form = $this->createForm(TaskType::class, $task = new Task());
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            //          Attribution de la tache à l'utilisateur enregistré
            /**
             * @var User $user
             */
            $user = $this->getUser();
            $task->setUser($user);
            $em->flush();
            $this->addFlash('success', 'La tâche a été bien été ajoutée.');
            return $this->redirectToRoute('task_list');
        }

        return $this->render('task/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/tasks/{id}/edit", name="task_edit")
     */
    public function editAction(Task $task, Request $request): RedirectResponse|Response
    {
        $this->denyAccessUnlessGranted('TASK_EDIT', $task, 'User tried to edit task without proper authorization');
        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'La tâche a bien été modifiée.');
            return $this->redirectToRoute('task_list');
        }

        return $this->render('task/edit.html.twig', [
            'form' => $form->createView(),
            'task' => $task,
        ]);
    }

    /**
     * @Route("/tasks/{id}/toggle", name="task_toggle")
     */
    public function toggleTaskAction(Task $task): RedirectResponse
    {
        $this->denyAccessUnlessGranted(
            'TASK_EDIT',
            $task,
            'User tried to toggle task status without proper authorization'
        );
        $task->toggle(!$task->isDone());
        $this->getDoctrine()->getManager()->flush();
        $this->addFlash('success', sprintf('La tâche %s a bien été marquée comme faite.', $task->getTitle()));
        return $this->redirectToRoute('task_list');
    }

    /**
     * @Route("/tasks/{id}/delete", name="task_delete")
     */
    public function deleteTaskAction(Task $task): RedirectResponse
    {
        $this->denyAccessUnlessGranted('TASK_EDIT', $task, 'User tried to delete task without proper authorization');
        $em = $this->getDoctrine()->getManager();
        $em->remove($task);
        $em->flush();
        $this->addFlash('success', 'La tâche a bien été supprimée.');
        return $this->redirectToRoute('task_list');
    }
}
