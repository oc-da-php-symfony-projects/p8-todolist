<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserController
 * @package App\Controller
 * @SuppressWarnings(PHPMD.ShortVariable)
 * @SuppressWarnings(PHPMD.ElseExpression)
 */
class UserController extends AbstractController
{
    /**
     * @Route("/users", name="user_list")
     */
    public function listAction(): Response
    {
        $this->denyAccessUnlessGranted(
            'USER_MANAGE',
            null,
            'User tried to access Users list without proper authorization'
        );
        return $this->render('user/list.html.twig', [
                'users' => $this->getDoctrine()->getRepository(User::class)->findAll(),
            ]);
    }

    /**
     * @Route("/users/create", name="user_create")
     */
    public function createAction(Request $request, UserPasswordEncoderInterface $encoder): RedirectResponse|Response
    {
        $this->denyAccessUnlessGranted('USER_MANAGE', null, 'User tried to create User without proper authorization');
        $form = $this->createForm(UserType::class, $user = new User());
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $password = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            if ('1' === $request->request->get('user')['admin']) {
                $user->setRoles(['ROLE_ADMIN']);
            }
            $user->setRoles(['ROLE_USER']);
            $em->flush();
            $this->addFlash('success', "L'utilisateur a bien été ajouté.");
            return $this->redirectToRoute('user_list');
        }

        return $this->render('user/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/users/{id}/edit", name="user_edit")
     */
    public function editAction(
        User $user,
        Request $request,
        UserPasswordEncoderInterface $encoder
    ): RedirectResponse|Response {
        $this->denyAccessUnlessGranted('USER_VIEW', $user, 'User tried to edit User without proper authorization');
        $form = $this->createForm(UserType::class, $user)->remove('password');
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $password = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            if ('1' === $request->request->get('user')['admin']) {
                $user->setRoles(['ROLE_ADMIN']);
            } else {
                $user->removeRole('ROLE_ADMIN');
                $user->setRoles(['ROLE_USER']);
            }
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', "L'utilisateur a bien été modifié");
            return $this->redirectToRoute('user_list');
        }

        return $this->render('user/edit.html.twig', ['form' => $form->createView(), 'user' => $user]);
    }
}
