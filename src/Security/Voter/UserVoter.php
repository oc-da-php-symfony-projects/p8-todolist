<?php

namespace App\Security\Voter;

use App\Entity\User;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

use function in_array;

class UserVoter extends Voter
{
    private Security $security;
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    #[Pure]
    protected function supports($attribute, mixed $subject): bool
    {
        return
            !empty($subject)
                ? in_array($attribute, ['USER_MANAGE', 'USER_VIEW'], true) && $subject instanceof User
                : in_array($attribute, ['USER_MANAGE', 'USER_VIEW'], true);
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
// if the user is anonymous, do not grant access
        if (!$this->security->isGranted('IS_AUTHENTICATED_FULLY', $user)) {
            return false;
        }
        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case 'USER_MANAGE':
                // logic to determine if the user can MANAGE

                if ($this->security->isGranted('ROLE_ADMIN')) {
                    return true;
                }

                break;
            case 'USER_VIEW':
                // logic to determine if the user can VIEW

                if (
                    $this->security->isGranted('ROLE_ADMIN'
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           /**
                        * @var User $subject
                        **/) || $subject === $user
                ) {
                    return true;
                }

                break;
        }
        return false;
    }
}
