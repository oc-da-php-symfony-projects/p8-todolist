<?php

namespace App\DataFixtures;

use App\Entity\Task;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class AppFixtures.
 */
class AppFixtures extends Fixture
{
    private UserPasswordEncoderInterface $encoder;
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * Load data fixtures with the passed EntityManager.
     * @SuppressWarnings(PHPMD)
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        $admin = new User();
        $admin->setUsername('TdlAdmin');
        $admin->setEmail('tdladminf@gmx.fr');
        $admin->setRoles(['ROLE_ADMIN']);
        $admin->setPassword($this->encoder->encodePassword($admin, 'TdlAdmin'));
        $manager->persist($admin);
        $testUser = new User();
        $testUser->setUsername('laura');
        $testUser->setEmail('laura@email.com');
        $testUser->setRoles(['ROLE_USER']);
        $testUser->setPassword($this->encoder->encodePassword($admin, 'laura'));
        $manager->persist($testUser);
        $testUserTask = new Task();
        $testUserTask->setTitle($faker->text(50));
        $testUserTask->setContent($faker->text());
        $testUserTask->setUser($testUser);
        $manager->persist($testUserTask);

        for ($i = 0; $i < 4; ++$i) {
            $user = new User();
            $user->setUsername($faker->username());
            $user->setEmail($faker->email());
            $manager->persist($user);
            $user->setRoles($user->getRoles());
            $user->setPassword($this->encoder->encodePassword($user, $user->getUsername()));
            for ($j = 0; $j < 4; ++$j) {
                $task = new Task();
                $task->setTitle($faker->text(50));
                $task->setContent($faker->text());
                $task->setUser($user);
                $manager->persist($task);
            }
            $manager->persist($user);
        }

        for ($i = 0; $i < 4; ++$i) {
            $task = new Task();
            $task->setTitle($faker->text(50));
            $task->setContent($faker->text());
            $manager->persist($task);
        }

        $manager->flush();
    }
}
