<?php

namespace App\Log;

use Monolog\Handler\FingersCrossed\ErrorLevelActivationStrategy;
use Symfony\Component\HttpKernel\Exception\HttpException;

use function in_array;

class LimitedErrorActivationStrategy extends ErrorLevelActivationStrategy
{
    public function __construct()
    {
        parent::__construct('error');
    }

    public function isHandlerActivated(array $record): bool
    {
        $isActivated = parent::isHandlerActivated($record);
        if (
            $isActivated
            && isset($record['context']['exception'])
            && $record['context']['exception'] instanceof HttpException
        ) {
            return !in_array($record['context']['exception']->getStatusCode(), [403, 404, 405], true);
        }

        return $isActivated;
    }
}
