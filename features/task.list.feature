Feature: Task List
  In order to organize my day
  As a todoer
  I need to see task list

  Background:
    Given the following people exist:
      | username  | email           | role | password |
      | Aslak     | aslak@email.com | 0    | Aslak    |
      | Joe       | joe@email.com   | 0    | Joe      |
      | Sara      | sara@email.org  | 1    | Sara     |
    Given the following tasks exist:
      | title  | content           | user      |
      | Setup app     | blaablabla | Aslak     |
      | Clean DB      | blaablabla |           |
      | Call Customer | blaablabla | Sara      |

  Scenario:
      Given user is on IndexPage