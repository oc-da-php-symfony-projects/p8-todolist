Feature: Login
  In order to access Task features
  As a ToDoer user or admin
  I need to be able to log in/out

  Background:
    Given user "Aslak" is registered with email "aslak@email.com" and password "Aslak"

  Scenario: Login
    Given user is on Login page
    When user log in as "Aslak" with "Aslak"
    Then user should be on TaskListPage